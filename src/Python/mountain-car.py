#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Based on https://gym.openai.com/docs/
import gym
import numpy as np
import random
import os
import time
import sys

import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d # For graph smoothing
from datetime import datetime # For timestamping csv files

# Initialise environment
env = gym.make("MountainCar-v0").env
env.reset()

# Mountain car has 3 possible actions:
# 0 = acclerate left
# 1 = do nothing
# 2 = acclerate right

# and two variables for state
# - state[0] is position
# - state[1] is velocity
# where
# - negativ means left of center
# - positiv means right of center


# Get lowest/highest values
min_pos, min_vel = env.observation_space.low  # [-1.2,-0.07]
max_pos, max_vel = env.observation_space.high # [ 0.6, 0.07]

# Number of discrete states per variable (car pos, car vel)
discrete_states_per_var = 30

# all combination of states between variables
state_space_n = discrete_states_per_var ** 2

# action space
all_actions = [0,1,2] # TODO don't let this be hardcoded
# state space
all_state_indeces = range(state_space_n) # TODO don't let this be hardcoded

# Number of rounds to train for
training_rounds = 2500

# Number rounds to consider for last X median
last_x = 1000


def is_positive(x):
    """Returns `True` if the given number is positive"""
    return x == abs(x)

def manual_run(render):
    """Manual (non-learned) aglrithm to solve the problem
    The car acclerates one way until its velocity direction changes.
    As soon as that happens the car starts acclerating the other way."""
    observation = env.reset()
    vel_positive = False
    move_left = True
    done = False
    steps=0

    while not done:
        steps+=1
        vel = observation[1]

        if render:
            env.render()

        vel_positive_old = vel_positive
        vel_positive = is_positive(vel)

        # If we start moving the other way, also accelerate that way
        if vel_positive != vel_positive_old:
            if move_left:
                action = 2
            else:
                action = 0
            move_left = not move_left
        # Otherwise keep moving that way
        else:
            if move_left:
                action = 0
            else:
                action = 2

        observation, reward, done, info = env.step(action)
    return steps
    #print(f"Episode finished after {t+1} timesteps")

def dummy_run_showcase():
    for i_episode in range(20):
        observation = env.reset()
        for t in range(1000):
            env.render()
            print(observation)
            action = env.action_space.sample()
            action = 0
            print(action)
            observation, reward, done, info = env.step(action)
            if done:
                print(f"Episode finished after {t+1} timesteps")
                break

def get_time_diff_as_str(start_time, finish_time):
    """Takes the start time and returns time passed in formatted string"""
    time_diff = finish_time - start_time

    minutes, seconds = divmod(time_diff, 60)
    hours, minutes = divmod(minutes, 60)

    return f"{hours:.0f}h {minutes:02.0f}m {seconds:02.0f}s"


# From https://gist.github.com/GeorgeSeif/5f1b2d650ce95efcc69d1fae77f0deca#file-scatter_plot-py (modified)
def lineplot(x_data, y_data, x_label="", y_label="", title="", filename=""):
    """Creates a line plot and displays or saves it"""
    # Create the plot object
    _, ax = plt.subplots()

    # Use a log y scale
    ax.set_yscale('log')


    # Plot the best fit line, set the linewidth (lw), color and
    # transparency (alpha) of the line
    ax.plot(x_data, y_data, lw = 2, color = '#539caf', alpha = 1)

    # Label the axes and provide a title
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    if not filename:
        plt.show()
    else:
        plt.savefig(filename)

# Inspired by:
# - https://gist.github.com/GeorgeSeif/5f1b2d650ce95efcc69d1fae77f0deca#file-scatter_plot-py
# - https://stackoverflow.com/a/4805456
def plot_learners(learner_list, filename=""):
    """Creates a line plot of the passed learners and displays or saves it"""

    title="Comparison of Learners on Mountain Car problem"
    x_label="Episodes"
    y_label="Time to complete problem"

    # Create the plot object
    _, ax = plt.subplots()

    # Use a log y scale
    ax.set_yscale('log')

    # Plot learners
    for learner in learner_list:
        ax.plot(range(len(learner.performance_history)), learner.performance_history)

    # Add legend
    ax.legend([learner.__class__.__name__ for learner in learner_list])

    # Provide a title and label the axes
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    # Either display plot or save it, depending on whether or not we passed a filename
    if not filename:
        plt.show()
    else:
        plt.savefig(filename+".eps")
        plt.savefig(filename+".png")

# Inspired by:
# - https://gist.github.com/GeorgeSeif/5f1b2d650ce95efcc69d1fae77f0deca#file-scatter_plot-py
# - https://stackoverflow.com/a/53472966
# - https://stackoverflow.com/a/4805456
def plot_learners_smooth(learner_list, filename="", σ=10):
    """Creates a smooth line plot of the passed learners and displays or saves it"""

    title = f"Comparison of Learners on Mountain Car problem (smoothed, σ={σ})"
    x_label = "Episodes"
    y_label = "Time to complete problem"


    # Create the plot object
    _, ax = plt.subplots()

    # Use a log y scale
    ax.set_yscale('log')

    # Plot learners
    for learner in learner_list:
        ysmoothed = gaussian_filter1d(learner.performance_history, sigma=σ)
        ax.plot(range(len(learner.performance_history)), ysmoothed)

    # Add legend
    ax.legend([learner.__class__.__name__ for learner in learner_list])

    # Provide a title and label the axes
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    # Either display plot or save it, depending on whether or not we passed a filename
    if not filename:
        plt.show()
    else:
        plt.savefig(filename+".eps")
        plt.savefig(filename+".png")

def write_out_learners_history_to_csv(learner_list, filename):
    """Writes the history, i.e. how long each episode took out to a CSV file for every learner"""
    # Make sure learner names are strings
    learner_names = [learner.__class__.__name__ for learner in learner_list]

    # Write data to csv
    np.savetxt(filename, np.array([learner.performance_history for learner in learner_list]).T, fmt='%d', header=','.join(learner_names), delimiter=',')


def bucket(x, min_x, max_x):
    """Gets the index of the bucket in which a certain value falls
    Returns a integer value between [0, discrete_states_per_var["""

    x = np.clip(x, min_x, max_x) # sets value to be in [min_x, max_x]
    x = (x-min_x)/(max_x-min_x)

    bucket_index = int(x*discrete_states_per_var)

    # If `x >= max_x`, then `bucket_index` will be `discrete_states_per_var` which is out of range
    # To prevent this, decrease `bucket_index` in this special case
    if bucket_index == discrete_states_per_var:
        bucket_index = discrete_states_per_var-1

    # ensure `bucket_index` is in range
    assert(0 <= bucket_index and bucket_index < discrete_states_per_var)

    return bucket_index

def get_state_index(state):
    """Gets index in Q state table corresponding to the current state"""
    global min_pos, min_vel
    global max_pos, max_vel

    # Extract variables from state
    pos, vel = state

    # Get discrete values
    pos = bucket(pos, min_pos, max_pos)
    vel = bucket(vel, min_vel, max_vel)

    # Get state_index from discrete values
    state_index = (
          pos * discrete_states_per_var**1
        + vel * discrete_states_per_var**0
    )
    return state_index


class SARSA_λ_Learner():
    def __init__(self, render=False, α=0.1, γ=0.6, ε=0.1, λ=0.5, cutoff=5000):
        self.render = render

        # Setup environment
        self.env = gym.make("MountainCar-v0").env

        # Hyperparameters
        self.α = α # learning rate
        self.γ = γ # discount factor
        self.ε = ε # greediness

        self.λ = λ # Backup weight

        self.cutoff = cutoff
        self.min_steps = cutoff

        # Initialise q table with zero values
        self.Q = np.zeros([state_space_n, self.env.action_space.n])

        # Record performance history
        self.performance_history = list()

    def choose_action_from(self, state):
        Q = self.Q
        ε = self.ε

        if random.uniform(0, 1) < ε: # Exploration
            # Check the action space
            action = self.env.action_space.sample()
        else:                        # Exploitation
            # Check the learned value
            action = np.argmax(Q[state]) # np.argmax returns index of max value
        return action

    def run_episode(self):
        Q = self.Q

        α = self.α
        γ = self.γ

        λ = self.λ

        global all_actions
        global all_state_indeces


        # E(s,a) = 0, for all s∊𝓢, a∊𝓐(s)
        E = np.zeros([state_space_n, self.env.action_space.n])

        # Initialise S, A
        S = get_state_index(self.env.reset())
        A = self.choose_action_from(S)

        steps = 0
        done = False
        # Repeat (for each step of episode) until S is terminal
        while not done and steps < self.cutoff:
            steps += 1
            if self.render: self.env.render()

            # Take action A, observe R, S'
            next_state_continuous, R, done, info = self.env.step(A)
            S_prime = get_state_index(next_state_continuous) # get index of our next state

            # Choose A' from S' using policy derived from Q (e.g. ε-greedy)
            A_prime = self.choose_action_from(S_prime)

            # δ <- R + γQ(S',A') - Q(S,A)
            δ = R + γ*Q[S_prime, A_prime] - Q[S,A]

            # E(S,A) = 1 (replacing traces)
            E[S,A] = 1

            # For all s∊𝓢,a∊𝓐(s):
            for s in all_state_indeces:
                for a in all_actions:

                    # Q(s,a) <- Q(s,a) + αδE(s,a)
                    Q[s,a] = Q[s,a] + α*δ*E[s,a]

                    # E(s,a) <- γλE(s,a)
                    E[s,a] = γ*λ*E[s,a]


            # S <- S'
            S = S_prime

            # A <- A'
            A = A_prime

        self.performance_history.append(steps)
        self.min_steps = min(steps, self.min_steps)

class Q_λ_Learner():
    def __init__(self, render=False, α=0.1, γ=0.6, ε=0.1, λ=0.5, cutoff=5000):
        self.render = render

        # Setup environment
        self.env = gym.make("MountainCar-v0").env

        # Hyperparameters
        self.α = α # learning rate
        self.γ = γ # discount factor
        self.ε = ε # greediness

        self.λ = λ # Backup weight

        self.cutoff = cutoff
        self.min_steps = cutoff

        # Initialise q table with zero values
        self.Q = np.zeros([state_space_n, self.env.action_space.n])

        # Record performance history
        self.performance_history = list()

    def choose_action_from(self, state):
        Q = self.Q
        ε = self.ε

        if random.uniform(0, 1) < ε: # Exploration
            # Check the action space
            action = self.env.action_space.sample()
        else:                        # Exploitation
            # Check the learned value
            action = np.argmax(Q[state]) # np.argmax returns index of max value
        return action

    def run_episode(self):
        Q = self.Q

        α = self.α
        γ = self.γ

        λ = self.λ


        global all_actions
        global all_state_indeces

        # Initialise e_trace # TODO docs, testing
        e_trace = np.zeros([state_space_n, self.env.action_space.n])

        # Init Vars
        steps = 0

        # Initialize s,a
        state = get_state_index(self.env.reset())
        action = self.choose_action_from(state)

        done = False

        # Repeat(for each step of epsiode)
        while not done and steps < self.cutoff:
            steps += 1

            if self.render: self.env.render()

            # Take action A, observe R, S'
            next_state_continuous, reward, done, info = self.env.step(action)
            S_prime = get_state_index(next_state_continuous) # get index of our next state

            # Choose A' from S' using policy derived from Q
            A_prime = self.choose_action_from(S_prime)

            # A* <- arg max_a Q(S',a)
            A_star = np.argmax(Q[S_prime]) # np.argmax returns index of max value
            # (if A' ties for max, then A* <- A')
            if Q[S_prime, A_star] == Q[S_prime, A_prime]: # TODO check if correct
                A_star = A_prime

            # δ <- R + γQ(S', A*) - Q(S,A)
            δ = reward + γ * Q[S_prime, A_star] - Q[state, action]

            # E(s,a) <- 1 (replacing traces)
            e_trace[state, action] = 1
            # For all s∊𝓢,a∊𝓐(s):
            for s in all_state_indeces:
                for a in all_actions:
                    # Q(s,a) <- Q(s,a) + αδe(s,a)
                    Q[s, a] = Q[s, a] + α*δ*e_trace[s, a]
                    # if A' = A*:
                    if A_prime == A_star:
                        # then E(s,a) <- γλE(s,a)
                        e_trace[s, a] = γ*λ* e_trace[s, a]
                    # else:
                    else:
                        # E(s,a) <- 0
                        e_trace[s, a] = 0
            # S = S'
            state = S_prime
            # A = A'
            action = A_prime

        self.performance_history.append(steps)
        if steps < self.min_steps:
            self.min_steps = steps

class SARSA_Learner():
    def __init__(self, render=False, α=0.1, γ=0.6, ε=0.1, cutoff=5000):
        self.render = render

        # Setup environment
        self.env = gym.make("MountainCar-v0").env

        # Hyperparameters
        self.α = α # learning rate
        self.γ = γ # discount factor
        self.ε = ε # greediness

        self.cutoff = cutoff
        self.min_steps = cutoff

        # Initialise q table with zero values
        self.Q = np.zeros([state_space_n, self.env.action_space.n])

        # Record performance history
        self.performance_history = list()

    def choose_action_from(self, state):
        Q = self.Q
        ε = self.ε

        if random.uniform(0, 1) < ε: # Exploration
            # Check the action space
            action = self.env.action_space.sample()
        else:                        # Exploitation
            # Check the learned value
            action = np.argmax(Q[state]) # np.argmax returns index of max value
        return action

    def run_episode(self):
        Q = self.Q

        α = self.α
        γ = self.γ

        # Initialise S, A
        S = get_state_index(self.env.reset())
        A = self.choose_action_from(S)

        steps = 0
        done = False
        # Repeat (for each step of episode) until S is terminal
        while not done and steps < self.cutoff:
            steps += 1
            if self.render: self.env.render()

            # Take action A, observe R, S'
            next_state_continuous, R, done, info = self.env.step(A)
            S_prime = get_state_index(next_state_continuous) # get index of our next state

            # Choose A' from S' using policy derived from Q (e.g. ε-greedy)
            A_prime = self.choose_action_from(S_prime)

            # Q(S,A) <- Q(S,A) + α[R + γQ(S',A') - Q(S,A)]
            δ = R + γ*Q[S_prime, A_prime] - Q[S,A]
            Q[S,A] = Q[S,A] + α*δ

            # S <- S'
            S = S_prime

            # A <- A'
            A = A_prime

        self.performance_history.append(steps)
        self.min_steps = min(steps, self.min_steps)

class Q_Learner():
    def __init__(self, render=False, α=0.1, γ=0.6, ε=0.1, cutoff=5000):
        self.render = render

        # Setup environment
        self.env = gym.make("MountainCar-v0").env

        # Hyperparameters
        self.α = α # learning rate
        self.γ = γ # discount factor
        self.ε = ε # greediness

        self.cutoff = cutoff
        self.min_steps = cutoff

        # Initialise q table with zero values
        self.Q = np.zeros([state_space_n, self.env.action_space.n])

        # Record performance history
        self.performance_history = list()


    def choose_action_from(self, state):
        Q = self.Q
        ε = self.ε

        if random.uniform(0, 1) < ε: # Exploration
            # Check the action space
            action = self.env.action_space.sample()
        else:                        # Exploitation
            # Check the learned value
            action = np.argmax(Q[state]) # np.argmax returns index of max value
        return action

    def run_episode(self):
        Q = self.Q

        α = self.α
        γ = self.γ

        # Initialise S
        state = get_state_index(self.env.reset())

        # Init Vars
        reward = 0
        steps = 0

        done = False
        # Repeat (for each step of episode) until S is terminal
        while not done and steps < self.cutoff:
            steps += 1

            if self.render: self.env.render()

            # Choose A from S using policy derived from Q
            action = self.choose_action_from(state)

            # Take action A, oberserve R, S'
            next_state_continuous, reward, done, info = self.env.step(action)
            next_state = get_state_index(next_state_continuous) # get index of our next state


            # Q(S,A) <- Q(S,A) + α[R+γ max_a Q(S',a) - Q(S,A)]
            Q[state, action] = (1 - α) * Q[state, action] + α * (reward + γ * np.max(Q[next_state]))

            # S <- S'
            state = next_state

        self.performance_history.append(steps)
        self.min_steps = min(steps, self.min_steps)

def print_mean_manual_run(episodes=1000):
    manual_run_history = list()
    for _ in range(episodes):
        manual_run_history.append(manual_run(False))
    print(f"Mean time for manual run: {np.mean(manual_run_history):0.3f}")


last_x_history = list()

#learner = Q_Learner()       # Q Learner
#learner = SARSA_Learner()   # SARSA Learner
#learner = Q_λ_Learner()     # Q(λ) Learner
learner = SARSA_λ_Learner() # SARSA(λ) Learner

if "--short-test" in sys.argv:
    print("Short Test")
    learners = [Q_Learner(), SARSA_Learner(), Q_λ_Learner(), SARSA_λ_Learner()]
    for learner in learners:
        print("=====================================================")
        print("Testing:", learner.__class__.__name__)
        print("=====================================================")
        start_time = time.time() # For measuring running time
        for i in range(5):
            learner.run_episode()
            print(f"Episode: {i:07} | Time passed {get_time_diff_as_str(start_time, time.time())} | min_time={learner.min_steps:4d}, mean={np.mean(learner.performance_history):4.3f}, last_{last_x}_mean={np.mean(learner.performance_history[-last_x:]):4.3f}")
        print("=====================================================")
        print("No reference error or similar for:", learner.__class__.__name__)
        print("=====================================================")
    # Finally plot the learners and save the plots to ensure correctness of called functions
    plot_learners(learners, "default")
    plot_learners_smooth(learners, "smooth", σ=5)
elif "--test" in sys.argv:
    print("Testing...")
    learners = [Q_Learner(), SARSA_Learner(), Q_λ_Learner(), SARSA_λ_Learner()]
    for learner in learners:
        print("=====================================================")
        print("Testing:", learner.__class__.__name__)
        print("=====================================================")
        start_time = time.time() # For measuring running time
        for i in range(training_rounds):
            learner.run_episode()
            if i % 50 == 0:
                print(f"Episode: {i:07} | Time passed {get_time_diff_as_str(start_time, time.time())} | min_time={learner.min_steps:4d}, mean={np.mean(learner.performance_history):4.3f}, last_{last_x}_mean={np.mean(learner.performance_history[-last_x:]):4.3f}")
        assert(learner.min_steps < 300)
        assert(np.mean(learner.performance_history) < 800)
        print("=====================================================")
        print("assert for", learner.__class__.__name__, "passed")
        print("=====================================================")
        print()

    # Get timestamped filename
    filename = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    # Finally, plot the learners, save the plots, and write out the performance history
    plot_learners(learners, filename="default "+filename)
    plot_learners_smooth(learners, filename="smooth "+filename, σ=5)
    write_out_learners_history_to_csv(learners, filename+".csv")

else:
    print("Starting...")
    print("Using:", learner)
    # Train model
    start_time = time.time() # For measuring running time
    last_update = start_time
    try:
        for i in range(training_rounds):
            learner.run_episode()

            # Print information every second
            if time.time() - last_update > 1:
                last_update = time.time()
                #os.system('clear') # Clears output

                print(f"Episode: {i:07} | Time passed {get_time_diff_as_str(start_time, time.time())} | min_time={learner.min_steps:4d}, mean={np.mean(learner.performance_history):4.3f}, last_{last_x}_mean={np.mean(learner.performance_history[-last_x:]):4.3f}")

                # Reduce learning rate if recent performance is better
                #if np.mean(hold_time_list[-200:]) < np.mean(hold_time_list[-1000:]): # TODO move to own function
                #    α = α / 10
                #    print("Reduced learning rate by 10, α is now:", α)

            if i % last_x == 0 and not i == 0:
                last_x_history.append(np.mean(learner.performance_history[-last_x:]))

        print("Training finished")
    except KeyboardInterrupt:
        print("Warning: Training stopped early by user!")

    finish_time = time.time()

    # Print the average duration for a manual run
    print_mean_manual_run()

    if not input("Press enter to show the plots: "):
        lineplot(range(len(learner.performance_history)), learner.performance_history,"cycles", "steps", f"Performance on MountainCar with {learner.__class__.__name__}, running for {get_time_diff_as_str(start_time, finish_time)}")
        #lineplot(range(len(last_x_history)), last_x_history,"x1000 cycles", "steps", f"Mean_1000 performance on MoutainCar with {learner}, running for {get_time_diff_as_str(start_time, finish_time)}")

    # Finally display
    learner.render = True
    while not input("Display run? ").startswith('q'):
        learner.run_episode()

    # Finally close the environment
    learner.env.close()
    env.close()