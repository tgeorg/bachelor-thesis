#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ctypes # To interact with C code

# Based on https://gym.openai.com/docs/
import gym
import numpy as np
#import random
#import os
#import time
#import sys
#
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d # For graph smoothing

from datetime import datetime # For timestamping csv files

x = 0
env = gym.make("MountainCar-v0").env

# Get lowest/highest values
min_pos, min_vel = env.observation_space.low  # [-1.2,-0.07]
max_pos, max_vel = env.observation_space.high # [ 0.6, 0.07]

def set_up_gym():
    global env

    #print("[Python] Setting up Gym")
    env = gym.make("MountainCar-v0").env

def reset_environment():
    global env

    state = env.reset()
    #print(tuple(state))
    #print("[Python] Environment reset")

    return tuple(state)

def some_test():
    global x
    print("x:",x)

def render_environment():
    global env
    env.render()
    print("[Python] rendered environment")


def env_step(action):
    global env
    assert(0 <= action)
    assert(action < 3)
    state, reward, done, info = env.step(action)

    #print("[Python] Took action:", action)
    return (state[0], state[1], reward, done)


discrete_states_per_var = 30

def bucket(x, min_x, max_x):
    """Gets the index of the bucket in which a certain value falls
    Returns a integer value between [0, discrete_states_per_var["""

    x = np.clip(x, min_x, max_x) # sets value to be in [min_x, max_x]
    x = (x-min_x)/(max_x-min_x)

    bucket_index = int(x*discrete_states_per_var)

    # If `x >= max_x`, then `bucket_index` will be `discrete_states_per_var` which is out of range
    # To prevent this, decrease `bucket_index` in this special case
    if bucket_index == discrete_states_per_var:
        bucket_index = discrete_states_per_var-1

    # ensure `bucket_index` is in range
    assert(0 <= bucket_index)
    assert(bucket_index < discrete_states_per_var)

    return bucket_index

def get_state_index(pos, vel):
#def get_state_index(state):
    """Gets index in Q state table corresponding to the current state"""
    global min_pos, min_vel
    global max_pos, max_vel

    # Extract variables from state
    #pos, vel = state

    # Get discrete values
    pos = bucket(pos, min_pos, max_pos)
    vel = bucket(vel, min_vel, max_vel)

    # Get state_index from discrete values
    state_index = (
          pos * discrete_states_per_var**1
        + vel * discrete_states_per_var**0
    )
    return state_index

def draw_single_graph_from_step_history(step_history, filename=""):
    """Creates a line plot of the passed learners and displays or saves it"""

    title="Learner performance on Mountain Car problem"
    x_label="Episodes"
    y_label="Time to complete problem"

    # Create the plot object
    _, ax = plt.subplots()

    # Use a log y scale
    ax.set_yscale('log')

    # Plot learners
    ax.plot(range(len(step_history)), step_history)

    # Add legend
    ax.legend("todo")

    # Provide a title and label the axes
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    # Either display plot or save it, depending on whether or not we passed a filename
    if not filename:
        plt.show()
    else:
        plt.savefig(filename)

def plot_learners(learner_names_list, learner_performance_list):
    """Creates a line plot of the passed learners and displays or saves it"""

    # Make sure learner names are strings
    learner_names_list = [str(x.decode()) for x in learner_names_list]

    title="Comparison of Learners on Mountain Car problem"
    x_label="Episodes"
    y_label="Time to complete problem"

    # Create the plot object
    _, ax = plt.subplots()

    # Use a log y scale
    ax.set_yscale('log')

    # Plot learners
    for learner_performance in learner_performance_list:
        ax.plot(range(len(learner_performance)), learner_performance)

    # Add legend
    ax.legend(learner_names_list)

    # Provide a title and label the axes
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    # Get timestamped filename
    filename = datetime.now().strftime('default %Y-%m-%d %H:%M:%S')
    
    # Save plot
    plt.savefig(filename+".png")
    plt.savefig(filename+".eps")

# Inspired by:
# - https://gist.github.com/GeorgeSeif/5f1b2d650ce95efcc69d1fae77f0deca#file-scatter_plot-py
# - https://stackoverflow.com/a/53472966
# - https://stackoverflow.com/a/4805456
def plot_learners_smooth(learner_names_list, learner_performance_list, σ=10):
    """Creates a smooth line plot of the passed learners and displays or saves it"""

    # Make sure learner names are strings
    learner_names_list = [str(x.decode()) for x in learner_names_list]

    title = f"Comparison of Learners on Mountain Car problem (smoothed, σ={σ})"
    x_label = "Episodes"
    y_label = "Time to complete problem"


    # Create the plot object
    _, ax = plt.subplots()

    # Use a log y scale
    ax.set_yscale('log')

    # Plot learners
    for learner_performance in learner_performance_list:
        ysmoothed = gaussian_filter1d(learner_performance, sigma=σ)
        ax.plot(range(len(learner_performance)), ysmoothed)

    # Add legend
    ax.legend(learner_names_list)

    # Provide a title and label the axes
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    # Get timestamped filename
    filename = datetime.now().strftime('smooth %Y-%m-%d %H:%M:%S')
    
    # Save plot
    plt.savefig(filename+".png")
    plt.savefig(filename+".eps")

def write_out_history_to_csv(learner_names_list, learner_performance_list):
    """Writes the history, i.e. how long each episode took out to a CSV file for every learner"""
    # Make sure learner names are strings
    learner_names_list = [str(x.decode()) for x in learner_names_list]

    # Get timestamped filename
    filename = datetime.now().strftime('%Y-%m-%d %H:%M:%S.csv')

    # Write data to csv
    np.savetxt(filename, np.array(learner_performance_list).T, fmt='%d', header=','.join(learner_names_list), delimiter=',')
