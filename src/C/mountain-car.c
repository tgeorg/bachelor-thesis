#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <stdbool.h>

//#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_double.h>

#define LIBRARY_NAME "mc-gym"

#define CUTOFF 5000

#define STATE_SPACE_N 30 * 30
#define ACTION_SPACE_N 3

/**
 * A struct that saves the state returned by the gym environment in doubles
 */
struct continous_state {
    double pos;
    double vel;
};

struct gym_return {
    struct continous_state state;
    int reward;
    bool done;
};

/**
 * Ensures the called functions exists in the active Python module by exiting if it doesn't
 */
void ensure_function_exists(PyObject *pFunc, char *function_name) {
    if (!pFunc || !PyCallable_Check(pFunc)) {
        if (PyErr_Occurred())
            PyErr_Print();
        fprintf(stderr, "Cannot find function \"%s\"\n", function_name);
        exit(EXIT_FAILURE);
    } else {
        return;
    }
}
/**
 * Returns a `PyObject` pointer to the Python function
 */
PyObject *get_pointer_to_Python_function(char *function_name, PyObject *pModule) {
    PyObject *pFunc = PyObject_GetAttrString(pModule, function_name);
    ensure_function_exists(pFunc, function_name);
    return pFunc;
}
/**
 * Exits the program if there was an error in the Python function call
 */
void exit_if_call_failed(PyObject *pValue) {
    if (pValue == NULL) {
        PyErr_Print();
        fprintf(stderr, "Call failed\n");
        exit(EXIT_FAILURE);
    } else {
        return;
    }
}

void set_up_gym(PyObject *pModule) {
    // Name of function to be called
    char *function_name = "set_up_gym";
    // Args to call function with
    PyObject *pArgs = NULL;

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);
}

struct continous_state reset_env(PyObject *pModule) {
    // Name of function to be called
    char *function_name = "reset_environment";
    // Args to call function with
    PyObject *pArgs = NULL;

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, NULL);

    exit_if_call_failed(pValue);

    PyTypeObject *type = pValue->ob_type;
    const char *p = type->tp_name;

    struct continous_state got_state = {PyFloat_AsDouble(PyTuple_GetItem(pValue, 0)),
                                        PyFloat_AsDouble(PyTuple_GetItem(pValue, 1))};

    return got_state;
}

void render_environment(PyObject *pModule) {
    // Name of function to be called
    char *function_name = "render_environment";
    // Args to call function with
    PyObject *pArgs = NULL;

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);

    // Print status
    printf("[C] Rendered environment\n");
}

struct gym_return env_step(PyObject *pModule, int action) {
    // Name of function to be called
    char *function_name = "env_step";
    // Args to call function with
    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, PyLong_FromLong(action));

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);

    PyTypeObject *type = pValue->ob_type;
    const char *p = type->tp_name;

    struct continous_state got_state = {PyFloat_AsDouble(PyTuple_GetItem(pValue, 0)),
                                        PyFloat_AsDouble(PyTuple_GetItem(pValue, 1))};

    int reward = PyLong_AsLong(PyTuple_GetItem(pValue, 2));
    int temp = PyObject_IsTrue(PyTuple_GetItem(pValue, 3));

    // == Could be removed in the future ==================================
    if (temp == -1) {
        printf("[C] `temp` was not the value we expected");
        exit(EXIT_FAILURE);
    }
    // ===================================================================

    bool done = (bool)temp;

    struct gym_return gym_ret = {got_state, reward, done};

    return gym_ret;
}

void dummy_run(PyObject *pModule) {

    // set up gym
    set_up_gym(pModule);

    //

    // reset gym
    reset_env(pModule);

    bool done = false;

    // Run until done for a max of `CUTOFF` iterations
    for (int i = 0; i < CUTOFF && !done; i++) {

        printf("[C] Taking step: %04d\n", i);

        // render state
        render_environment(pModule);

        // step
        int action = rand() % 3; // get action ∊ {0,1,2}

        // take step and get state, reward, done
        struct gym_return gym_ret = env_step(pModule, action);

        // Print information for debugging purposes
        printf("[C]\n - pos: %f\n - vel: %f\n - reward: %d\n - done: %s\n", gym_ret.state.pos, gym_ret.state.vel,
               gym_ret.reward, gym_ret.done ? "true" : "false");

        // Update done
        done = gym_ret.done;
    }
}

int get_state_index(struct continous_state ct, PyObject *pModule) {
    // TODO currently this gets handled in Python, move to C for the future

    // Name of function to be called
    char *function_name = "get_state_index";
    // Args to call function with
    PyObject *pArgs = PyTuple_New(2);
    PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(ct.pos));
    PyTuple_SetItem(pArgs, 1, PyFloat_FromDouble(ct.vel));

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);

    // PyTypeObject *type = pValue->ob_type;
    // const char *p = type->tp_name;

    int state_index = PyLong_AsLong(pValue);

    return state_index;
}

int get_index_of_max_action(int state, gsl_matrix *q_table) {
    // Set value to first entry for initialisation
    int action = 0;
    double val_of_max_action = gsl_matrix_get(q_table, state, action);

    // Get the value of the maximum possible future action
    int n;
    for (n = 0; n < ACTION_SPACE_N; ++n) {
        double new_val_of_max_action = gsl_matrix_get(q_table, state, n);
        if (new_val_of_max_action > val_of_max_action) {
            val_of_max_action = new_val_of_max_action;
            action = n;
        }
    }
    return action;
}

int choose_action_from_state(int state, gsl_matrix *q_table) {
    // Greediness factor
    double epsilon = 0.1; // TODO make easier to adjust

    int action;

    double random_double = (double)rand() / (double)(RAND_MAX);

    if (random_double < epsilon) {
        // Exploration:  Check the action space
        action = rand() % ACTION_SPACE_N;
    } else {
        // Exploitation: Check the learned values
        action = get_index_of_max_action(state, q_table);
    }

    // return action
    return action;
}

double get_max_action(int state, gsl_matrix *q_table) {
    double max_action = gsl_matrix_get(q_table, state, 0); // Set initial state to first value

    // Get the value of the maximum possible future action
    for (int n = 0; n < ACTION_SPACE_N; ++n) {
        double new_max_action = gsl_matrix_get(q_table, state, n);
        if (new_max_action > max_action) {
            max_action = new_max_action;
        }
    }
    return max_action;
}

int q_learner(PyObject *pModule, gsl_matrix *q_table) {
    // Hyperparameters
    double alpha = 0.1;
    double gamma = 0.6;

    // Reset gym
    int state = get_state_index(reset_env(pModule), pModule);

    // Variables for tracking state and progress
    bool done = false;
    int steps;

    // Repeat (for each step of episode) until S is terminal
    for (steps = 0; steps < CUTOFF && !done; steps++) {

        // render_environment(pModule);
        // TODO uncomment: render_environment(pModule);

        // Choose A from S using policy derived from Q
        int action = choose_action_from_state(state, q_table);

        // Take action A, observe R, S'
        struct gym_return gym_ret = env_step(pModule, action);
        int state_prime = get_state_index(gym_ret.state, pModule);
        int reward = gym_ret.reward;
        done = gym_ret.done;

        // Q(S,A) <- Q(S,A) + α[R+γ max_a Q(S',a) - Q(S,A)]
        // Calculate new value...
        double q_update = alpha * (reward + gamma * get_max_action(state_prime, q_table));
        double q_new_value = (1 - alpha) * gsl_matrix_get(q_table, state, action) + q_update;
        // ...and set it in Q-Table
        gsl_matrix_set(q_table, state, action, q_new_value);

        // S <- S'
        state = state_prime;
    }
    return steps;
}

int sarsa_learner(PyObject *pModule, gsl_matrix *q_table) {
    // Hyperparameters
    double alpha = 0.1;
    double gamma = 0.6;

    // Initialise S, A
    int state = get_state_index(reset_env(pModule), pModule);
    int action = choose_action_from_state(state, q_table);

    // Variables for tracking state and progress
    bool done = false;
    int steps;

    // Repeat (for each step of episode) until S is terminal
    for (steps = 0; steps < CUTOFF && !done; steps++) {

        // TODO uncomment: render_environment(pModule);

        // Take action A, observe R, S'
        struct gym_return gym_ret = env_step(pModule, action);
        int state_prime = get_state_index(gym_ret.state, pModule);
        int reward = gym_ret.reward;
        done = gym_ret.done;

        // Choose A' from S' using policy derived from Q (e.g. ε-greedy)
        int action_prime = choose_action_from_state(state_prime, q_table);

        // Q(S,A) <- Q(S,A) + α[R + γQ(S',A') - Q(S,A)]
        // Calculate new value...
        double delta = reward + gamma * gsl_matrix_get(q_table, state_prime, action_prime) -
                       gsl_matrix_get(q_table, state, action);
        double q_new_value = gsl_matrix_get(q_table, state, action) + alpha * delta;
        // ...and set it in Q-Table
        gsl_matrix_set(q_table, state, action, q_new_value);

        // S <- S'
        state = state_prime;
        // A <- A'
        action = action_prime;
    }
    return steps;
}

int sarsa_lambda_learner(PyObject *pModule, gsl_matrix *q_table) {
    // Hyperparameters
    double alpha = 0.1;
    double gamma = 0.6;
    double lambda = 0.5;

    // E(s,a) <- 0, for all s∊𝓢, a∊𝓐(s)
    gsl_matrix *e_trace = gsl_matrix_calloc(STATE_SPACE_N, ACTION_SPACE_N);

    // Initialise S, A
    int state = get_state_index(reset_env(pModule), pModule);
    int action = choose_action_from_state(state, q_table);

    // Variables for tracking state and progress
    bool done = false;
    int steps;

    // Repeat (for each step of episode) until S is terminal
    for (steps = 0; steps < CUTOFF && !done; steps++) {

        // TODO uncomment: render_environment(pModule);

        // Take action A, observe R, S'
        struct gym_return gym_ret = env_step(pModule, action);
        int state_prime = get_state_index(gym_ret.state, pModule);
        int reward = gym_ret.reward;
        done = gym_ret.done;

        // Choose A' from S' using policy derived from Q (e.g. ε-greedy)
        int action_prime = choose_action_from_state(state_prime, q_table);

        // δ <- R + γQ(S',A') - Q(S,A)
        double delta = reward + gamma * gsl_matrix_get(q_table, state_prime, action_prime) -
                       gsl_matrix_get(q_table, state, action);

        // E(S,A) <- 1 (replacing traces)
        gsl_matrix_set(e_trace, state, action, 1);

        // For all s∊𝓢,a∊𝓐(s):
        for (int s = 0; s < STATE_SPACE_N; s++) {
            for (int a = 0; a < ACTION_SPACE_N; a++) {
                // Q(s,a) <- Q(s,a) + αδE(s,a)
                double q_update = alpha * delta * gsl_matrix_get(e_trace, s, a);
                double new_q_value = gsl_matrix_get(q_table, s, a) + q_update;
                gsl_matrix_set(q_table, s, a, new_q_value);

                // E(s,a) <- γλE(s,a)
                double new_e_value = gamma * lambda * gsl_matrix_get(e_trace, s, a);
                gsl_matrix_set(e_trace, s, a, new_e_value);
            }
        }
        // S <- S'
        state = state_prime;
        // A <- A'
        action = action_prime;
    }
    gsl_matrix_free(e_trace); // free memory used by e_trace
    return steps;
}

int q_lambda_learner(PyObject *pModule, gsl_matrix *q_table) {
    // Hyperparameters
    double alpha = 0.1;
    double gamma = 0.6;
    double lambda = 0.5;

    // E(s,a) = 0, for all s∊𝓢, a∊𝓐(s)
    gsl_matrix *e_trace = gsl_matrix_calloc(STATE_SPACE_N, ACTION_SPACE_N);

    // Initialise S, A
    int state = get_state_index(reset_env(pModule), pModule);
    int action = choose_action_from_state(state, q_table);

    // Variables for tracking state and progress
    bool done = false;
    int steps;

    // Repeat (for each step of episode) until S is terminal
    for (steps = 0; steps < CUTOFF && !done; steps++) {

        // TODO uncomment: render_environment(pModule);

        // Take action A, observe R, S'
        struct gym_return gym_ret = env_step(pModule, action);
        int state_prime = get_state_index(gym_ret.state, pModule);
        int reward = gym_ret.reward;
        done = gym_ret.done;

        // Choose A' from S' using policy derived from Q (e.g. ε-greedy)
        int action_prime = choose_action_from_state(state_prime, q_table);
        // A* <- arg max_a Q(S',a)
        int action_star = get_index_of_max_action(state_prime, q_table);

        // (if A' ties for max, then A* <- A')
        if (gsl_matrix_get(q_table, state_prime, action_star) == gsl_matrix_get(q_table, state_prime, action_prime)) {
            action_star = action_prime;
        }

        // δ <- R + γQ(S', A*) - Q(S,A)
        double delta =
            reward + gamma * gsl_matrix_get(q_table, state_prime, action_star) - gsl_matrix_get(q_table, state, action);

        // E(S,A) <- 1 (replacing traces)
        gsl_matrix_set(e_trace, state, action, 1);

        // For all s∊𝓢,a∊𝓐(s):
        for (int s = 0; s < STATE_SPACE_N; s++) {
            for (int a = 0; a < ACTION_SPACE_N; a++) {
                // Q(s,a) <- Q(s,a) + αδE(s,a)
                double new_q_value = gsl_matrix_get(q_table, s, a) + alpha * delta * gsl_matrix_get(e_trace, s, a);
                gsl_matrix_set(q_table, s, a, new_q_value);
                // if A' = A*:
                if (action_prime == action_star) {
                    // then E(s,a) <- γλE(s,a)
                    double new_e_value = gamma * lambda * gsl_matrix_get(e_trace, s, a);
                    gsl_matrix_set(e_trace, s, a, new_e_value);
                }
                // else:
                else {
                    // E(s,a) <- 0
                    gsl_matrix_set(e_trace, s, a, 0);
                }
            }
        }
        // S <- S'
        state = state_prime;
        // A <- A'
        action = action_prime;
    }
    gsl_matrix_free(e_trace); // free memory used by e_trace
    return steps;
}

int min_value_in_array(int *array, int array_len) {
    int min = array[0];
    for (int i = 0; i < array_len; i++) {
        if (array[i] < min)
            min = array[i];
    }
    return min;
}

double mean_of_array(int *array, int array_len) {
    int sum = 0;
    for (int i = 0; i < array_len; i++) {
        sum += array[i];
    }
    return (sum * 1.0) / array_len;
}

PyObject *make_python_list_from_int_array(int array[], size_t size) {
    PyObject *l = PyList_New(size);
    for (size_t i = 0; i != size; ++i) {
        PyList_SET_ITEM(l, i, PyLong_FromLong(array[i]));
    }
    return l;
}

PyObject *make_python_list_from_python_object_array(PyObject *array[], size_t size) {
    PyObject *l = PyList_New(size);
    for (size_t i = 0; i != size; ++i) {
        PyList_SET_ITEM(l, i, array[i]);
    }
    return l;
}

PyObject *make_python_list_from_string_array(char *array[], size_t size) {
    PyObject *l = PyList_New(size);
    for (size_t i = 0; i != size; ++i) {

        // Pointer to C string data
        char *s = array[i];
        // Length of data
        int len = strlen(array[i]);

        // Make a bytes object
        PyObject *obj = Py_BuildValue("y#", s, len);

        PyList_SET_ITEM(l, i, obj);
    }
    return l;
}

void plot_learners(PyObject *list_of_learner_names, PyObject *list_of_learners, PyObject *pModule) {

    // Name of function to be called
    char *function_name = "plot_learners";
    // Args to call function with
    PyObject *pArgs = PyTuple_New(2);
    PyTuple_SetItem(pArgs, 0, list_of_learner_names);
    PyTuple_SetItem(pArgs, 1, list_of_learners);

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);

    // PyTypeObject *type = pValue->ob_type;
    // const char *p = type->tp_name;

    return;
}

void plot_learners_smooth(PyObject *list_of_learner_names, PyObject *list_of_learners, PyObject *pModule) {

    // Name of function to be called
    char *function_name = "plot_learners_smooth";
    // Args to call function with
    PyObject *pArgs = PyTuple_New(2);
    PyTuple_SetItem(pArgs, 0, list_of_learner_names);
    PyTuple_SetItem(pArgs, 1, list_of_learners);

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);

    // PyTypeObject *type = pValue->ob_type;
    // const char *p = type->tp_name;

    return;
}

void draw_single_graph(PyObject *python_list_step_history, PyObject *pModule) {

    // Name of function to be called
    char *function_name = "draw_single_graph_from_step_history";
    // Args to call function with
    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, python_list_step_history);

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);

    // PyTypeObject *type = pValue->ob_type;
    // const char *p = type->tp_name;

    return;
}

void write_out_history_to_csv(PyObject *list_of_learner_names, PyObject *list_of_learners, PyObject *pModule) {

    // Name of Python function to be called
    char *function_name = "write_out_history_to_csv";
    // Args to call function with
    PyObject *pArgs = PyTuple_New(2);
    PyTuple_SetItem(pArgs, 0, list_of_learner_names);
    PyTuple_SetItem(pArgs, 1, list_of_learners);

    // Get pointer to the Python function
    PyObject *pFunc = get_pointer_to_Python_function(function_name, pModule);

    // Call the function
    PyObject *pValue = PyObject_CallObject(pFunc, pArgs);

    //
    exit_if_call_failed(pValue);

    // PyTypeObject *type = pValue->ob_type;
    // const char *p = type->tp_name;

    return;
}

int main(int argc, char *argv[]) {
    PyObject *pName, *pModule, *pFunc;
    PyObject *pArgs, *pValue;

    Py_Initialize();

    // =============================================
    // So we can actually run local python files
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append(\".\")");
    // =============================================

    pName = PyUnicode_DecodeFSDefault(LIBRARY_NAME); // File name (without the `.py`)

    pModule = PyImport_Import(pName);

    if (pModule == NULL) {
        PyErr_Print();
        fprintf(stderr, "Failed to load \"%s\"\n", LIBRARY_NAME);
        return EXIT_FAILURE;
    }

    // set up gym
    set_up_gym(pModule);

    if (argc > 1) {
        int (*learners[4])(PyObject *, gsl_matrix *) = {q_learner, sarsa_learner, q_lambda_learner,
                                                        sarsa_lambda_learner};
        char *learners_name[4] = {"Q_Learner", "SARSA_Learner", "Q_λ_Learner", "SARSA_λ_Learner"};
        PyObject *learners_history[4];

        if (strcmp(argv[1], "--test") == 0) {
            printf("Running test...\n");
            for (int i = 0; i < sizeof(learners) / sizeof(learners[0]); i++) {
                printf("=====================================================\n");
                printf("Testing: %s\n", learners_name[i]);
                printf("=====================================================\n");

                // Allocate Q-Table
                gsl_matrix *q_table = gsl_matrix_alloc(STATE_SPACE_N, ACTION_SPACE_N);
                // Set all elements of Q-Table to zero
                gsl_matrix_set_zero(q_table);

                int total_episodes_per_learner = 2500;
                int learner_performance_history[total_episodes_per_learner];

                for (int j = 0; j < total_episodes_per_learner; j++) {
                    int latest_run_steps = learners[i](pModule, q_table);

                    // Save latest episode
                    learner_performance_history[j] = latest_run_steps;
                    if (j % 50 == 0) {
                        printf("Iteration %04d | Latest %04d | min_steps=%04d | mean=%04.2f \n", j, latest_run_steps,
                               min_value_in_array(learner_performance_history, j + 1),
                               mean_of_array(learner_performance_history, j + 1));
                    }
                }
                // Free q_table when done
                gsl_matrix_free(q_table);

                // Checks
                int min_steps = min_value_in_array(learner_performance_history, total_episodes_per_learner);
                double mean = mean_of_array(learner_performance_history, total_episodes_per_learner);
                assert(min_steps < 300);
                assert(mean < 800);

                printf("min_steps = %d\n", min_steps);
                printf("mean = %f\n", mean);

                printf("=====================================================\n");
                printf("assert for %s passed\n", learners_name[i]);
                printf("=====================================================\n");

                learners_history[i] =
                    make_python_list_from_int_array(learner_performance_history, total_episodes_per_learner);
            }
            PyObject *python_list_learners_history = make_python_list_from_python_object_array(learners_history, 4);
            PyObject *python_list_learners_name = make_python_list_from_string_array(learners_name, 4);
            write_out_history_to_csv(python_list_learners_name, python_list_learners_history, pModule);
            plot_learners(python_list_learners_name, python_list_learners_history, pModule);
            plot_learners_smooth(python_list_learners_name, python_list_learners_history, pModule);
        }
        if (strcmp(argv[1], "--short-test") == 0) {
            printf("Running short test...\n");
            for (int i = 0; i < sizeof(learners) / sizeof(learners[0]); i++) {
                printf("=====================================================\n");
                printf("Testing: %s\n", learners_name[i]);
                printf("=====================================================\n");

                // Allocate Q-Table
                gsl_matrix *q_table = gsl_matrix_alloc(STATE_SPACE_N, ACTION_SPACE_N);
                // Set all elements of Q-Table to zero
                gsl_matrix_set_zero(q_table);

                for (int j = 0; j < 5; j++) {
                    int latest_run_steps = learners[i](pModule, q_table);
                    printf("Iteration %04d. Latest run took %04d steps\n", j, latest_run_steps);
                }
                // Free q_table when done
                gsl_matrix_free(q_table);
                printf("=====================================================\n");
                printf("No reference error or similar for: %s\n", learners_name[i]);
                printf("=====================================================\n");
            }
        }

    } else {
        // Allocate Q-Table
        gsl_matrix *q_table = gsl_matrix_alloc(STATE_SPACE_N, ACTION_SPACE_N);
        // Set all elements of Q-Table to zero
        gsl_matrix_set_zero(q_table);

        int (*learner)(PyObject *, gsl_matrix *) = q_learner;
        // int (*learner)(PyObject *, gsl_matrix *) = q_lambda_learner;
        // int (*learner)(PyObject *, gsl_matrix *) = sarsa_learner;
        // int (*learner)(PyObject *, gsl_matrix *) = sarsa_lambda_learner;
        int total_episodes = 2000;
        int step_history[total_episodes];
        for (int i = 0; i < total_episodes; i++) {
            int latest_run_steps = learner(pModule, q_table);
            step_history[i] = latest_run_steps;
            printf("Iteration %04d. Latest run took %04d steps\n", i, latest_run_steps);
        }
        PyObject *python_list_step_history = make_python_list_from_int_array(step_history, total_episodes);
        draw_single_graph(python_list_step_history, pModule);
    }
    // dummy_run(pModule);

    if (Py_FinalizeEx() < 0) {
        return 120;
    }
    return 0;
}
