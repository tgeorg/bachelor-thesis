#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# %%

# For data handling
import numpy as np
import pandas as pd

# For getting files
import glob

# For plotting
import matplotlib.pyplot as plt
# For graph smoothing
from scipy.ndimage.filters import gaussian_filter1d

# Plot limits
plots_y_limit = [170, 5500]


# %%
def remove_hash_space(my_input):
    # Removes the `# ` left my numpy comment
    return my_input if "# " not in my_input else my_input[2:]

# %%
def plot_learners(df, sub_ax=None, filename="", multiple_runs=1, colours=None, plots_y_limit=None):
    """Creates a line plot of the passed learners and displays or saves it"""

    title = f"(runs={multiple_runs})"
    if not sub_ax:
        title = "Comparison of Learners on Mountain Car problem " + title

    x_label = "Episodes"
    y_label = "Time to complete problem (steps)"

    # Create the plot object
    ax = df.plot(ax=sub_ax, color=colours, legend=(not bool(sub_ax)))

    # Use a log y scale
    ax.set_yscale('log')

    # Set y_limit if given
    if plots_y_limit:
        ax.set_ylim(plots_y_limit)

    # Provide a title and label the axes
    ax.set_title(title)
    if not sub_ax:
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)

    # Either display plot or save it, depending on whether or not we passed a filename
    if filename:
        plt.savefig(filename+".eps")
        plt.savefig(filename+".png")

# %%
def plot_learners_smooth(df, sub_ax=None, filename="", σ=10, multiple_runs=1, colours=None, plots_y_limit=None):
    """Creates a smooth line plot of the passed learners and displays or saves it"""


    title = f"(smoothed, σ={σ}, runs={multiple_runs})"
    if not sub_ax:
        title = "Comparison of Learners on Mountain Car problem " + title
    x_label = "Episodes"
    y_label = "Time to complete problem (steps)"

    df = df.apply(gaussian_filter1d, sigma=σ)

    # Create the plot object
    ax = df.plot(ax=sub_ax, color=colours, legend=(not bool(sub_ax)))

    # Use a log y scale
    ax.set_yscale('log')

    # Set y_limit if given
    if plots_y_limit:
        ax.set_ylim(plots_y_limit)

    # Provide a title and label the axes
    ax.set_title(title)
    if not sub_ax:
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)

    # Either display plot or save it, depending on whether or not we passed a filename
    if filename:
        plt.savefig(filename+".eps")
        plt.savefig(filename+".png")


# %%
folder = "data/mountain-car/logs"
log_list = glob.glob(folder + '/*.csv')

# %%
# Read data into pandas dataframes...
df_list = [pd.read_csv(log) for log in log_list]
# ...and remove the `# ` at the start of the name of the first column
[df.rename(remove_hash_space, axis='columns', inplace=True) for df in df_list]

# Print length of dataframe list
len(df_list)

# %%
df_sum = pd.DataFrame(0, index=np.arange(len(df_list[0])), columns=df_list[0].columns.values)
for df in df_list:
    df_sum = df_sum.add(df)
df_sum = df_sum / len(df_list)
df_sum
# %%
plot_learners(df_sum, multiple_runs=len(df_list), colours=['C0', 'C2', 'C1', 'C3'], plots_y_limit=plots_y_limit)
plot_learners(df_sum.loc[:, ['Q_Learner', 'Q_λ_Learner']], multiple_runs=len(df_list), colours=['C0', 'C1'], plots_y_limit=plots_y_limit)
plot_learners(df_sum.loc[:, ['SARSA_Learner', 'SARSA_λ_Learner']], multiple_runs=len(df_list), colours=['C2', 'C3'], plots_y_limit=plots_y_limit)

# %%
plot_learners_smooth(df_sum, multiple_runs=len(df_list), colours=['C0', 'C2', 'C1', 'C3'], plots_y_limit=plots_y_limit)
plot_learners_smooth(df_sum.loc[:, ['Q_Learner', 'Q_λ_Learner']], multiple_runs=len(df_list), colours=['C0', 'C1'], plots_y_limit=plots_y_limit)
plot_learners_smooth(df_sum.loc[:, ['SARSA_Learner', 'SARSA_λ_Learner']], multiple_runs=len(df_list), colours=['C2', 'C3'], plots_y_limit=plots_y_limit)

# %%
plot_learners(df_list[0], colours=['C0', 'C2', 'C1', 'C3'])
plot_learners_smooth(df_list[0], colours=['C0', 'C2', 'C1', 'C3'])

# %%

plt.figure(dpi=200)

fig, axs = plt.subplots(3, 3)

fig.set_size_inches(14, 14)

#fig.tight_layout()

fig.suptitle('Comparison of Learners on Mountain Car problem', fontsize=20)

# Make enough space between plots
plt.subplots_adjust(hspace=0.4)

# Plot learners
plot_learners(df_sum, multiple_runs=len(df_list), colours=['C0', 'C2', 'C1', 'C3'], plots_y_limit=plots_y_limit, sub_ax=axs[0, 0])
plot_learners(df_sum.loc[:, ['Q_Learner', 'Q_λ_Learner']], multiple_runs=len(df_list), colours=['C0', 'C1'], plots_y_limit=plots_y_limit, sub_ax=axs[1, 0])
plot_learners(df_sum.loc[:, ['SARSA_Learner', 'SARSA_λ_Learner']], multiple_runs=len(df_list), colours=['C2', 'C3'], plots_y_limit=plots_y_limit, sub_ax=axs[2, 0])

plot_learners_smooth(df_sum, multiple_runs=len(df_list), colours=['C0', 'C2', 'C1', 'C3'], plots_y_limit=plots_y_limit, sub_ax=axs[0, 1])
plot_learners_smooth(df_sum.loc[:, ['Q_Learner', 'Q_λ_Learner']], multiple_runs=len(df_list), colours=['C0', 'C1'], plots_y_limit=plots_y_limit, sub_ax=axs[1, 1])
plot_learners_smooth(df_sum.loc[:, ['SARSA_Learner', 'SARSA_λ_Learner']], multiple_runs=len(df_list), colours=['C2', 'C3'], plots_y_limit=plots_y_limit, sub_ax=axs[2, 1])

plot_learners_smooth(df_sum, multiple_runs=len(df_list), colours=['C0', 'C2', 'C1', 'C3'], plots_y_limit=plots_y_limit, sub_ax=axs[0, 2])
plot_learners_smooth(df_sum.loc[:, ['Q_Learner', 'Q_λ_Learner']], multiple_runs=len(df_list), colours=['C0', 'C1'], plots_y_limit=plots_y_limit, sub_ax=axs[1, 2])
plot_learners_smooth(df_sum.loc[:, ['SARSA_Learner', 'SARSA_λ_Learner']], multiple_runs=len(df_list), colours=['C2', 'C3'], plots_y_limit=plots_y_limit, sub_ax=axs[2, 2])

# Set limits to zoom in
axs[0, 2].set_xlim([1000, 2600])
axs[0, 2].set_ylim([190, 290])
axs[1, 2].set_xlim([1000, 2600])
axs[1, 2].set_ylim([190, 290])
axs[2, 2].set_xlim([1000, 2600])
axs[2, 2].set_ylim([190, 290])

# Make zoomed plots linear
axs[0, 2].set_yscale('linear')
axs[1, 2].set_yscale('linear')
axs[2, 2].set_yscale('linear')

# Add legend
handles, labels = axs[0,0].get_legend_handles_labels()
plt.legend(handles, labels, loc='lower center', ncol=4, bbox_to_anchor=(-0.66, -0.5, 0, 0), fontsize=15)

# Add y-label
axs[1,0].set_ylabel("Time to complete problem (steps)", fontsize=15)

# Add x-label
fig.text(0.5, 0.08, "Episodes", ha='center', fontsize=15)

# Make directory if it does not exist
import pathlib
pathlib.Path('plots').mkdir(parents=True, exist_ok=True) 

# Save
plt.savefig("plots/mc_learners_9.eps")
#Show
plt.show()


# %%
extra_folder = "data/mountain-car/extra (lambda=0)"
extra_log_list = glob.glob(extra_folder + '/*.csv')
len(extra_log_list)
# %%
# Read data into pandas dataframes...
extra_df_list = [pd.read_csv(log) for log in extra_log_list]
# ...and remove the `# ` at the start of the name of the first column
[df.rename(remove_hash_space, axis='columns', inplace=True) for df in extra_df_list]

# Print length of dataframe list
len(extra_df_list)

# %%
extra_df_sum = pd.DataFrame(0, index=np.arange(len(extra_df_list[0])), columns=extra_df_list[0].columns.values)
for df in extra_df_list:
    extra_df_sum = extra_df_sum.add(df)
extra_df_sum = extra_df_sum / len(extra_df_list)
extra_df_sum.rename(columns = {'Q_λ_Learner':'Q_λ=0_Learner', 'SARSA_λ_Learner':'SARSA_λ=0_Learner'}, inplace = True) 
extra_df_sum
# %%

plt.figure(dpi=200)
fig, axs = plt.subplots(1, 2) # (rows col)
fig.set_size_inches(10, 8)

multiple_runs = 100
σ=10

title = f"smoothed, σ={σ}, runs={multiple_runs}"
x_label = "Episodes"
y_label = "Time to complete problem (steps)"

# Left plot
# Create the plot object
df_sum.apply(gaussian_filter1d, sigma=σ).plot(ax=axs[0], color=['C0', 'C2', 'C1', 'C3'], legend=False)
extra_df_sum.apply(gaussian_filter1d, sigma=σ).plot(ax=axs[0], color=['C4','C5'], legend=False)
# Use a log y scale
axs[0].set_yscale('log')
# Provide a title and label the axes
axs[0].set_title(title)
axs[0].set_xlabel(x_label)
axs[0].set_ylabel(y_label)

# Right plot
# Create the plot object
df_sum.apply(gaussian_filter1d, sigma=σ).plot(ax=axs[1], color=['C0', 'C2', 'C1', 'C3'])
extra_df_sum.apply(gaussian_filter1d, sigma=σ).plot(ax=axs[1], color=['C4','C5'])
# Set limits to zoom in
axs[1].set_xlim([1000, 2600])
axs[1].set_ylim([190, 290])
# Provide a title and label the axes
axs[1].set_title(title+" (zoomed)")
axs[1].set_xlabel(x_label)

# Set figure title
fig.suptitle("Comparison with λ=0 Learners on Mountain Car problem", fontsize=20)

fig.tight_layout()

# Save...
plt.savefig("plots/mc_learners_lambda=0.eps")
# ...and show
plt.show()

# %%
