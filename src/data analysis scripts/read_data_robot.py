#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# %%

"""
Learner indexing
"""
# 1 "Q"
# 2 "Q_lambda"
# 3 "SARSA"
# 4 "SARSA_lambda"

"""
Reward function indexing
"""
# 1 "Power Only"
# 2 "Error Only"
# 3 "Power and Error"
# 4 "Power, Error, Replan"
# 5 "Power, Error, Rampling Replan"
# 6 "Weighted Power_Error (1:10)"
# 7 "Error and 10-Sample Moving Power Average"

"""
Imports
"""
# For data handling
import numpy as np
import pandas as pd

# For getting files
import glob

# For plotting
import matplotlib.pyplot as plt
# For graph smoothing
from scipy.ndimage.filters import gaussian_filter1d

# To show tables more nicely
from IPython.display import display

"""
Globals
"""
TRAINING_STRING = "training"
EVALUATION_STRING = "evaluation"
reward_functions = ["Power Only", "Error Only", "Power and Error", "Power, Error, Replan", "Power, Error, Rampling Replan", "Weighted Power_Error (1:10)", "Error and 10-Sample Moving Power Average"]
learners = ["Q", "Q_lambda", "SARSA", "SARSA_lambda"]

# %%
# Get all the logs
folder = "data/robot"
subfolder_list = glob.glob(folder + '/*')
log_list = list()
for subfolder in subfolder_list:
    log_list += glob.glob(subfolder + '/*.csv')
    # log_list += glob.glob(subfolder + '/bad logs/*.csv')
len(log_list)

#%%
def separate_into_train_eval(files):
    """
    Separates logs into training and evaluation based on whether they contain `TRAINING_STRING` or `EVALUATION_STRING`
    """
    training_logs = [file_name for file_name in files if TRAINING_STRING in file_name]
    evaluation_logs = [file_name for file_name in files if EVALUATION_STRING in file_name]
    return training_logs, evaluation_logs

training_logs, evaluation_logs = separate_into_train_eval(log_list)

#%%
def get_ondemand_logs():
    """Returns a list logs that were created from running `robot_controller` with the default Linux power governor"""
    return [log for log in log_list if "ondemand" in log]

#%%
def filter_learner_reward_func(reward_function, learner, file_name):
    """Returns true if `file_name` contains `reward_function` and `learner`"""
    return reward_function in file_name and learner in file_name

#%%
def turn_to_struct(files):
    """
    Turns the list of logs into a struct index by struct[reward_function][learner] = `list of logs`
    """
    index_struct = dict()
    for reward_function in reward_functions:
        index_struct[reward_function] = dict()
        for learner in learners:
            index_struct[reward_function][learner] = [file_name for file_name in files if filter_learner_reward_func(reward_function, learner+" ", file_name)] # The added trailing space is necessary as to not classify `X_lambda` as `X`
            # Filter out potentially bad based on too short run
            index_struct[reward_function][learner] = [item for item in index_struct[reward_function][learner] if pd.read_csv(item)["time"].max() > 30]
    return index_struct

training_struct = turn_to_struct(training_logs)
evaluation_struct = turn_to_struct(evaluation_logs)

#%%
def create_table(index_struct):
    """
    Shows the available logs in a table with reward functions plotted against learners
    """
    df = pd.DataFrame(index=reward_functions, columns=learners)
    for reward_function in reward_functions:
        for learner in learners:
            df.at[reward_function, learner] = len(index_struct[reward_function][learner])
    return df
#%%
create_table(training_struct).to_latex(
    buf="plots/training_samples_table.tex",
    bold_rows=True,
    caption="Number of training samples per learner / reward function combination",
    label="tab:number_of_training_samples",
    column_format="|r|r|r|r|r|"
)
create_table(training_struct)

#%%
int(create_table(training_struct).sum().sum())

#%%
create_table(evaluation_struct).to_latex(
    buf="plots/evaluation_samples_table.tex",
    bold_rows=True,
    caption="Number of evaluation samples per learner / reward function combination",
    label="tab:number_of_evaluation_samples",
    column_format="|r|r|r|r|r|"
)
create_table(evaluation_struct)

#%%
int(create_table(evaluation_struct).sum().sum())

# %%

def print_summary(current_file):

    import pandas as pd

#    print("reading: ", current_file)
    log = pd.read_csv(current_file)


    # Filter all extreme values (most likely caused by false sensor readings)
    log = log[log["xpos"] <  30000]
    log = log[log["xpos"] > -30000]
    log = log[log["ypos"] <  30000]
    log = log[log["ypos"] > -30000]
    log = log[log["dist_from_path"] < 2000]

    # Inaccurate measurement of arrival, ignore
    # reached_destination = log["right_vel"].iloc[-1] == 0 and log["left_vel"].iloc[-1] == 0
    time = log["time"].max()
    dist_t = log["distT"].max()
    rms_dist_from_path = ((log["dist_from_path"])**2).mean() ** (0.5)
    max_dist_from_path = log["dist_from_path"].max()
    avg_odroid_power = log["iOdroid"].mean()
    avg_big_freq = log["commanded_big_freq"].mean()/1000000
    avg_little_freq = log[log["commanded_little_freq"]>0]["commanded_little_freq"].mean()/1000000
    max_q_diff = log["q_diff"].max()
    avg_q_diff = log["q_diff"].mean()
    avg_reward = log["inst_reward"].mean()
    sum_reward = sum(log["inst_reward"])

    freq_k_p = log["FREQ_K_P"].iloc[0]
    freq_k_e = log["FREQ_K_E"].iloc[0]

    #print(log["little_freq"].min(), log["little_freq"].max())
#    print("results:")
    governor = log["big_governor"].iloc[-1]
    # print("reached destination: ", reached_destination)
#    print("time: " , str(time) + " s")
#    print("distance travelled: ", str(dist_t)+" mm")
#    print("rms distance from path: ", str(rms_dist_from_path)+" mm")
#    print("max distance from path: ", str(max_dist_from_path)+" mm")
#    print("average odroid power: ", str(avg_odroid_power)+" A")
#    print("big freq avg: ", str(avg_big_freq)+" GHz")
#    print("little freq avg: ", str(avg_little_freq)+" GHz")
#    print("governor: ", governor)
#    print("q_diff max: ", max_q_diff)
#    print("q_diff avg: ", avg_q_diff)
#    print("reward avg: ", avg_reward)
#    print("reward sum: ", sum_reward)

    filename= current_file.split('/')[-1]


    # Insert new line
#    print()
    # TODO temporary
    return (filename, governor, avg_big_freq, avg_little_freq, avg_odroid_power, time, dist_t, rms_dist_from_path, max_dist_from_path, freq_k_p, freq_k_e,avg_q_diff,avg_reward)

# %%

# Work through all collected files
# for log in evaluation_logs:
#     print_summary(log)

# %%
new_struct = dict()
for learner in learners:
    learner_df = pd.DataFrame()
    for reward_function in reward_functions:
        my_list = list()
        for log in evaluation_struct[reward_function][learner]:
            my_list.append(print_summary(log))
            #filename, governor, avg_big_freq, avg_little_freq, avg_odroid_power, time, dist_t, rms_dist_from_path, max_dist_from_path, freq_k_p, freq_k_e,avg_q_diff,avg_reward = print_summary(log)
        df = pd.DataFrame(my_list, columns=["filename", "governor", "avg_big_freq", "avg_little_freq", "avg_odroid_power", "time", "dist_t", "rms_dist_from_path", "max_dist_from_path", "freq_k_p", "freq_k_e","avg_q_diff","avg_reward"])

        mean_df = pd.DataFrame(df.mean()).T

        # Add standard deviation for path error, power, and time
        mean_df["std_rms_dist_from_path"] = df["rms_dist_from_path"].std()
        mean_df["std_avg_odroid_power"]   = df["avg_odroid_power"].std()
        mean_df["std_time"]               = df["time"].std()

        # Add the number of runs
        mean_df.insert(0, "Runs", [len(evaluation_struct[reward_function][learner])])

        # Add the type of reward function
        mean_df.insert(0, "Reward function", [reward_function])

        mean_df.set_index("Reward function", inplace=True)

        #display(mean_df)
        #print(len(evaluation_struct[reward_function][learner]))
        learner_df = learner_df.append(mean_df)
        #new_struct[learner]
    learner_df.name = learner
    print(learner_df.name)
    display(learner_df)
    new_struct[learner] = learner_df

#%%
def get_ondemand_df():
    ondemand_logs = get_ondemand_logs()
    my_list = list()
    for ondemand_log in ondemand_logs:
        my_list.append(print_summary(ondemand_log))
    ondemand_df = pd.DataFrame(my_list, columns=["filename", "governor", "avg_big_freq", "avg_little_freq", "avg_odroid_power", "time", "dist_t", "rms_dist_from_path", "max_dist_from_path", "freq_k_p", "freq_k_e","avg_q_diff","avg_reward"])
    ondemand_mean_df = pd.DataFrame(ondemand_df.mean()).T

    # Add standard deviation for path error, power, and time
    ondemand_mean_df["std_rms_dist_from_path"] = ondemand_df["rms_dist_from_path"].std()
    ondemand_mean_df["std_avg_odroid_power"]   = ondemand_df["avg_odroid_power"].std()
    ondemand_mean_df["std_time"]               = ondemand_df["time"].std()

    ondemand_mean_df.insert(0, "Runs", [len(ondemand_logs)])
    ondemand_mean_df.insert(0, "Reward function", ["ondemand"])
    ondemand_mean_df.set_index("Reward function", inplace=True)
    return ondemand_mean_df

display(get_ondemand_df())


# %%

def create_comparison_plot(test_df):

    hatches = ['//', '\\\\', 'oo','xxx', 'xx','x', '**', '////','\\\\\\']
    colors = ['b','r','y','#27ae60','#1e8449','#145a32','#d35400','c','w']

    """
    performance
    """

    f = plt.figure(num=0, figsize=(8.5,5.5), dpi=100)
    f.suptitle(test_df.name, y = 1.05)

    test_df = test_df.append(get_ondemand_df())


    ax1 = f.add_subplot(1,3,1)
    #ax1.bar([0, 0.5, 1, 1.5, 2.0, 2.5, 3, 3.5], averages[2,:], 0.5, tick_label=experiments)
    ax1.bar(range(0,len(test_df.index)), test_df["rms_dist_from_path"].values/test_df.at["ondemand", "rms_dist_from_path"], 1, tick_label=test_df.index.values)
    plt.xticks(rotation=90)
    ax1.set_title('RMS Path Error')
    ax1.set_ylabel('Relative RMS Path Error')

    for i, bar in enumerate(ax1.patches):
        bar.set_hatch(hatches[i])
        bar.set_color(colors[i])
        bar.set_ec('k')

    # Add horizontal line on 1
    ax1.axhline(y=1, color="black", linestyle='dashed', linewidth=0.5)

    ax2 = f.add_subplot(1,3,2)
    ax2.bar(range(0,len(test_df.index)), test_df["avg_odroid_power"].values/test_df.at["ondemand", "avg_odroid_power"], 1, tick_label=test_df.index.values) # TODO make sure it's power and NOT current
    plt.xticks(rotation=90)
    ax2.set_title('Average Power')
    ax2.set_ylabel('Relative Power')

    for i, bar in enumerate(ax2.patches):
        bar.set_hatch(hatches[i])
        bar.set_color(colors[i])
        bar.set_ec('k')

    # Add horizontal line on 1
    ax2.axhline(y=1, color="black", linestyle='dashed', linewidth=0.5)

    ax3 = f.add_subplot(1,3,3)
    ax3.bar(range(0,len(test_df.index)), test_df["time"].values/test_df.at["ondemand", "time"], 1, tick_label=test_df.index.values)
    plt.xticks(rotation=90)
    ax3.set_title('Run Time')
    ax3.set_ylabel('Relative Execution Time')

    for i, bar in enumerate(ax3.patches):
        bar.set_hatch(hatches[i])
        bar.set_color(colors[i])
        bar.set_ec('k')

    # Add horizontal line on 1
    ax3.axhline(y=1, color="black", linestyle='dashed', linewidth=0.5)

    f.tight_layout()

    plt.show()
    #plt.savefig("metrics_1_q.eps", format="eps")


for learner in learners:
    create_comparison_plot(new_struct[learner])

# %%

fig, axs = plt.subplots(3, len(learners)) # (rows col)
fig.set_size_inches(9, 7)
hatches = ['//', '\\\\', 'oo','xxx', 'xx','x', '**', '////','\\\\\\']
colors = ['b','r','y','#27ae60','#1e8449','#145a32','#d35400','c','w']

for j, learner in enumerate(learners):

    test_df = new_struct[learner]

    # Add ondemand measurements
    test_df = test_df.append(get_ondemand_df())

    # Path distance error
    axs[0, j].bar(
        range(0,len(test_df.index)),
        test_df["rms_dist_from_path"].values/test_df.at["ondemand", "rms_dist_from_path"],
        1,
        yerr=test_df["std_rms_dist_from_path"].values/test_df.at["ondemand", "rms_dist_from_path"]
    )
    axs[0, j].set_xticks([])

    #  Power usage
    axs[1, j].bar(
        range(0,len(test_df.index)),
        test_df["avg_odroid_power"].values/test_df.at["ondemand", "avg_odroid_power"],
        1,
        yerr=test_df["std_avg_odroid_power"].values/test_df.at["ondemand", "avg_odroid_power"]
        )
    axs[1, j].set_xticks([])

    # Time to complete
    axs[2, j].bar(
        range(0,len(test_df.index)),
        test_df["time"].values/test_df.at["ondemand", "time"],
        1,
        yerr=test_df["std_time"].values/test_df.at["ondemand", "time"],
        )
    axs[2, j].set_xticks([])

# Colour all the bars
for j, learner in enumerate(learners):
    for row in range(3):
        # Colour the bars
        for i, bar in enumerate(axs[row, j].patches):
            bar.set_hatch(hatches[i])
            bar.set_color(colors[i])
            bar.set_ec('k')
        # ...and add horizontal lines on y=1
        axs[row, j].axhline(y=1, color="black", linestyle='dashed', linewidth=0.5)


# Set tight layout
fig.tight_layout()

for i, learner in enumerate(learners):
    axs[0, i].set_title(learner)

axs[0, 0].set_ylabel('Relative RMS Path Error')#, loc='top') # if this gives you an error, make sure your version of matplotlib >= 3.3.1
axs[1, 0].set_ylabel('Relative Power')#, loc='top')
axs[2, 0].set_ylabel('Relative Execution Time')#, loc='top')


from matplotlib.patches import Patch
fig.legend(
    [Patch(facecolor=x[0], hatch=x[1], edgecolor='black',label='Color Patch') for i, x in enumerate(zip(colors, hatches))], # gives us the coloured patches
    test_df.index.values,
    loc="lower center",
    ncol=4,
    bbox_to_anchor=(.5, -.12)
)

# Keep all error plots on same y
axs[0,0].set_ylim(0,1.4)
axs[0,1].set_ylim(0,1.4)
axs[0,2].set_ylim(0,1.4)
axs[0,3].set_ylim(0,1.4)

# Keep all power plots on same y
axs[1,0].set_ylim(0,1.05)
axs[1,1].set_ylim(0,1.05)
axs[1,2].set_ylim(0,1.05)
axs[1,3].set_ylim(0,1.05)

# Keep all time plots on same y
axs[2,0].set_ylim(0,1.2)
axs[2,1].set_ylim(0,1.2)
axs[2,2].set_ylim(0,1.2)
axs[2,3].set_ylim(0,1.2)

fig.suptitle("Performance comparison of the 4 learners", y=1.1, fontsize=20)

# Save...
plt.savefig("plots/comparison_4_learners_robot.eps", format="eps", bbox_inches="tight")
# ...and show
plt.show()

# %%
"""
Plot learners per metric
"""


def make_metric_plot_4(metric="", metric_name="", y_limit=None, filename="", title=""):
    hatches = ['//', '\\\\', 'oo','xxx', 'xx','x', '**', '////','\\\\\\']
    colors = ['b','r','y','#27ae60','#1e8449','#145a32','#d35400','c','w']

    qm = {
        0:(0,0),
        1:(0,1),
        2:(1,0),
        3:(1,1)
    }

    fig, axs = plt.subplots(2, 2) # (rows col)
    fig.set_size_inches(9, 9)

    for j, learner in enumerate(learners):


        test_df = new_struct[learner]

        # Add ondemand measurements
        test_df = test_df.append(get_ondemand_df())

        # Path distance error
        axs[qm[j][0], qm[j][1]].bar(
            range(0,len(test_df.index)),
            test_df[metric].values/test_df.at["ondemand", metric],
            1,
            yerr=test_df["std_"+metric].values/test_df.at["ondemand", metric]
        )
        axs[qm[j][0], qm[j][1]].set_xticks([])

    # Colour all the bars
    for j, learner in enumerate(learners):
        # Colour the bars
        for i, bar in enumerate(axs[qm[j][0], qm[j][1]].patches):
            bar.set_hatch(hatches[i])
            bar.set_color(colors[i])
            bar.set_ec('k')
        # ...and add horizontal lines on y=1
        axs[qm[j][0], qm[j][1]].axhline(y=1, color="black", linestyle='dashed', linewidth=0.5)



    for i, learner in enumerate(learners):
        axs[qm[i][0], qm[i][1]].set_title(learner)

    axs[0, 0].set_ylabel(metric_name)#, loc='top') # if this gives you an error, make sure your version of matplotlib >= 3.3.1
    axs[1, 0].set_ylabel(metric_name)#, loc='top') # if this gives you an error, make sure your version of matplotlib >= 3.3.1

    from matplotlib.patches import Patch
    fig.legend(
        [Patch(facecolor=x[0], hatch=x[1], edgecolor='black',label='Color Patch') for i, x in enumerate(zip(colors, hatches))], # gives us the coloured patches
        test_df.index.values,
        loc="lower center",
        ncol=4,
        bbox_to_anchor=(.5, -.12)
    )

    # Keep all error plots on same y
    for i in range(4):
        axs[qm[i][0], qm[i][1]].set_ylim(0, y_limit)

    fig.suptitle(title, y=1.1, fontsize=20)

    # Set tight layout
    fig.tight_layout(pad=1)

    # Save...
    plt.savefig(filename, format="eps", bbox_inches="tight")
    # ...and show
    plt.show()

make_metric_plot_4(metric="rms_dist_from_path", metric_name="Relative RMS Path Error", y_limit=1.4, filename="plots/comp_4_learners_robot_nr1_path.eps", title="Path error comparison of the 4 learners")
make_metric_plot_4(metric="avg_odroid_power", metric_name="Relative Power", y_limit=1.05, filename="plots/comp_4_learners_robot_nr2_power.eps", title="Power comparison of the 4 learners")
make_metric_plot_4(metric="time", metric_name="Relative Execution Time", y_limit=1.2, filename="plots/comp_4_learners_robot_nr3_time.eps", title="Runtime comparison of the 4 learners")


#%%

def moving_average(data, window):
    data_s = np.empty(data.size)
    for i in range(0, data.size):
        if i > (data.size - window):
            data_s[i] = data_s[i-1]
            continue
        data_s[i] = data[i]/window
        for j in range(1, window):
            data_s[i] += data[i+j] / window
    return data_s

"""
Time Series
"""
def make_time_series_plot(pd_df, plt_num, title="", offset=0):
    fig = plt.figure(num=plt_num, figsize=(12,5), dpi=200)
    ax1 = fig.add_subplot(221+offset)
    ax2 = ax1.twinx()

    t = pd_df['time']
    pm = pd_df['power_mode']
    iOdroid = pd_df['iOdroid']
    reward = pd_df['inst_reward']
    i_mean = np.average(iOdroid)
    i_mean = np.ones(iOdroid.shape) * i_mean
    dist = pd_df['dist_from_path']

    dist_mean = np.average(dist)
    dist_mean = np.ones(dist.shape) * dist_mean

    dist_mva = moving_average(dist, 5)
    # dist_n = dist_mva / max(dist_mva)
    i_mva = moving_average(iOdroid, 5)


    replan = (pd_df['dastar_replanning'])

    replan_active = replan.copy()
    replan_inactive = replan.copy()
    replan_active[replan_active < 1] = np.nan
    replan_inactive[replan_inactive == 1] = np.nan
    replan_active = replan_active - 1
    ax1.plot(t, replan_active, 'red', label='Replanning Active', linewidth=5.0)
    ax1.plot(t, replan_inactive, 'k', label='Replanning Inactive', linewidth=0.5)
    #ax1.plot(t, pm, label='Power Mode')
    ax1.plot(t, i_mva, label='Current')
    ax1.plot(t, i_mean, 'b:', linewidth=0.5)
    # TODO: ax1.set_xlim(0, max(t))
    ax2.plot(t, dist, 'green', label='Error')
    ax2.plot(t, dist_mean, 'g:', linewidth=0.5)
    ax2.set_xlim(0, max(t))
    ax1.set_ylim(-0.1, 2.3)
    # ax2.set_ylim(-50, max(dist_mva)+20)
    ax2.set_ylim(-50, 700)

    #ax1.text(0.5, 1.3, 'Maximum Frequency Governor $f_{big} = 2.0 GHz,  f_{little} = 1.6 GHz$', horizontalalignment='center',
    #        transform=ax1.transAxes)
    ax1.set_title(title)

    # bbox = [x0, y0, x1, y1]
    # TODO Adjust the `-0.4` to change vertical height of legend
    if not offset:
        ax1.legend(bbox_to_anchor=(0., -0.4, 1., .102), loc=2, ncol=5, borderaxespad=0.)
    else:
        ax2.legend(bbox_to_anchor=(0.75, -0.4, .2, .102), loc=2, ncol=4, mode="expand", borderaxespad=0.)

    #ax1.set_ylabel('Relative RMS Path Error')
    # ax1.set_xlabel('Time (s)')
    if not offset:
        ax1.set_ylabel('Current (A)')
    else:
        ax2.set_ylabel('Error (mm)')

    plt.subplots_adjust(hspace = 0.5)

    ax3 = fig.add_subplot(223+offset)
    #ax3.plot(t, iOdroid, label='Current')
    ax3.plot(t, reward,'y', label='Reward' )

    # Plot mean reward
    rew_mean = np.average(reward)
    rew_mean = np.ones(reward.shape) * rew_mean
    ax3.plot(t, rew_mean, 'k:', linewidth=0.5)

    ax3.set_xlim(0, max(t))
    ax3.set_ylim(0, 2.1)
    ax3.set_xlabel('Time (s)')
    if not offset:
        ax3.set_ylabel('Reward')
    if not offset:
        ax3.legend(bbox_to_anchor=(0., -0.35, 1., .102), loc=2, ncol=5, borderaxespad=0.)

sel_Q_df = pd.read_csv("data/robot/5th_batch/Q - Error and 10-Sample Moving Power Average - evaluation 2020-08-04 01:48:43.csv")
sel_Q_λ_df = pd.read_csv("data/robot/7th_batch/Q_lambda - Error and 10-Sample Moving Power Average - evaluation 2020-08-05 16:02:44.csv")

make_time_series_plot(sel_Q_df, 0, title="Q-Learner\nPower, Error, and Ramping Replan Reward Function")
make_time_series_plot(sel_Q_λ_df, 0, title="Q-Learner(λ)\nPower, Error, and Ramping Replan Reward Function", offset=1)
plt.tight_layout()
plt.savefig("plots/comparison_q-ql_timeseries.eps", format="eps", bbox_inches="tight")
plt.show()

# %%
def create_subbar_comp_plot(df1, df2, metric=None, ax=None, unit=None):
    width = 0.35 # the width of the bars
    mult = 3 if metric == "avg_odroid_power" else 1
    # Add bars
    ax.bar(df1.index.values, df1[metric].values*mult, -width, align='edge', color='C0', label=df1.name, edgecolor="black", yerr=df1["std_"+metric].values*mult)
    ax.bar(df2.index.values, df2[metric].values*mult, +width, align='edge', color='C1', label=df2.name, edgecolor="black", yerr=df2["std_"+metric].values*mult)
    # Set y label
    ax.set_ylabel(f"{metric} ({unit})")


fig, axs = plt.subplots(1, 3) # (rows col)
fig.set_size_inches(13, 3)
q = new_struct["Q"]
ql= new_struct["Q_lambda"]
metrics = ["rms_dist_from_path", "avg_odroid_power", "time"]
units = ["mm", "W", "s"]
for i, metric in enumerate(metrics):
    create_subbar_comp_plot(q, ql, metric=metric, ax=axs[i], unit=units[i])

for ax in fig.axes:
    plt.sca(ax)
    plt.xticks(rotation=90)

axs[0].set_title("Path error")
axs[1].set_title("Power")
axs[2].set_title("Time")

fig.suptitle("Performance comparison between Q and Q(λ)", y=1.1, fontsize=20)

# Set tight layout
fig.tight_layout()

handles, labels = axs[0].get_legend_handles_labels()
fig.legend(handles, labels, loc='lower center', ncol=2)

# Save...
plt.savefig("plots/comparison_q-ql_bars.eps", format="eps", bbox_inches="tight")
# ...and show
plt.show()

# %%

def make_reward_plot(pd_df, pos, f_lr, title="", colour="", xlabel="",  ylabel="", scale=3, show_tick_params=False, hide_yticks = False):
    ax = f_lr.add_subplot(pos[0], pos[1], pos[2])
    l1, = ax.plot(pd_df['time'], abs(pd_df['q_diff'])*scale, colour, linewidth=0.25)
    ax.set_xlim(0, 650)
    ax.set_ylim(-1,20)
    if not show_tick_params:
        # Hide x values
        ax.tick_params(axis='x', labelcolor='none')
    if hide_yticks:
        # Hide y values
        ax.tick_params(axis='y', labelcolor='none')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title, fontsize=10)

    # Get replanning sections
    replan = (pd_df['dastar_replanning'])

    # Show active replanning in graph
    replan_active = replan.copy()
    replan_active[replan_active < 1] = np.nan
    # Move line "below" plot
    replan_active = replan_active - 2
    # And plot
    ax.plot(pd_df['time'], replan_active, 'red', label='Replanning Active', linewidth=5.0)

    # ...and add horizontal lines on y=1
    mean = (abs(pd_df['q_diff'])*scale).mean()
    ax.axhline(y=mean, color="black", linestyle='dashed', linewidth=0.5, label='Test')
    import matplotlib.transforms as transforms
    trans = transforms.blended_transform_factory(ax.get_yticklabels()[0].get_transform(), ax.transData)
    ax.text(1.05,
        mean,
        f"{mean:.03f}",
        color="black",
        transform=trans,
        ha="left",
        va="center"
    )
def plot_training_rewards(
        f_lr = None,
        ql_pow_only            = None,
        ql_err_only            = None,
        ql_pow_err             = None,
        ql_pow_err_replan      = None,
        ql_pow_err_replan_ramp = None,
        ql_weighted_pow_1_10   = None,
        ql_moving_avg          = None,
        offset = 0
    ):
    """
    Plot rewards
    """
    # ax1, ax2, ax3, ax4, ax5, ax6, ax7 = None, None, None, None, None, None, None
    replan_active = None
    replan_inactive = None

    # Make enough space between plots
    plt.subplots_adjust(hspace = 0.4)

    #f_lr.suptitle('Learning Rates of Different Reward Functions', fontsize=18)

    # TODO move `f_lr` out of function and make general

    # Power Only
    make_reward_plot(ql_pow_only, (7, 2, 1+offset), f_lr, title="Power Only Reward", colour="blue", hide_yticks=offset)
    # Error Only
    make_reward_plot(ql_err_only, (7, 2, 3+offset), f_lr, title="Error Only Reward", colour="m", hide_yticks=offset)
    # Power plus Error
    make_reward_plot(ql_pow_err, (7, 2, 5+offset), f_lr, title="Power and Error Reward", colour="k", scale=3/2, hide_yticks=offset)
    # Power Error Replan
    make_reward_plot(ql_pow_err_replan, (7, 2, 7+offset), f_lr, title="Power, Error, and Replan Reward", colour="c", scale=1, ylabel='' if offset else 'Q Difference', hide_yticks=offset)
    # Power Error Replan Ramp
    make_reward_plot(ql_pow_err_replan_ramp, (7, 2, 9+offset), f_lr, title="Power, Error, and Ramping Replan Reward", colour="green", scale=1, hide_yticks=offset)
    # Power Error (1:10)
    make_reward_plot(ql_weighted_pow_1_10, (7, 2, 11+offset), f_lr, title="Weighted Power/Error (1:10) Reward", colour="tab:olive", hide_yticks=offset)
    # Moving Average
    make_reward_plot(ql_moving_avg, (7, 2, 13+offset), f_lr, title="Error and 10-Sample Moving Power Average Reward", colour="tab:gray", scale=3/2, xlabel="Time (s)", show_tick_params=True, hide_yticks=offset)

    # Save plots as file
    #plt.savefig("learning_rates_7.eps", bbox_inches='tight', format="eps")


f_lr = plt.figure(num=100, figsize=(8.5,13), dpi=200)

plot_training_rewards(
    f_lr = f_lr,
    ql_pow_only            = pd.read_csv("data/robot/7th_batch/Q - Power Only - training 2020-08-05 09:36:10.csv"),
    ql_err_only            = pd.read_csv("data/robot/3rd_batch/Q - Error Only - training 2020-08-01 16:38:46.csv"),
    ql_pow_err             = pd.read_csv("data/robot/2nd_batch/Q - Power and Error - training 2020-07-31 21:40:49.csv"),
    ql_pow_err_replan      = pd.read_csv("data/robot/3rd_batch/Q - Power, Error, Replan - training 2020-08-01 18:35:47.csv"),
    ql_pow_err_replan_ramp = pd.read_csv("data/robot/3rd_batch/Q - Power, Error, Rampling Replan - training 2020-08-01 19:47:02.csv"),
    ql_weighted_pow_1_10   = pd.read_csv("data/robot/9th_batch/Q - Weighted Power_Error (1:10) - training 2020-08-07 12:24:36.csv"),
    ql_moving_avg          = pd.read_csv("data/robot/7th_batch/Q - Error and 10-Sample Moving Power Average - training 2020-08-05 15:35:33.csv"),
    offset=0
)

extra_logs = [log for log in log_list if "training" in log and "extra" in log and "Q_lambda" in log]
plot_training_rewards(
    f_lr = f_lr,
    ql_pow_only            = pd.read_csv("data/robot/extra/Q_lambda - Power Only - training 2020-08-29 14:08:42.csv"),
    ql_err_only            = pd.read_csv("data/robot/extra/Q_lambda - Error Only - training 2020-08-29 14:24:26.csv"),
    ql_pow_err             = pd.read_csv("data/robot/extra/Q_lambda - Power and Error - training 2020-08-29 12:19:59.csv"),
    ql_pow_err_replan      = pd.read_csv("data/robot/extra/Q_lambda - Power, Error, Replan - training 2020-08-29 20:17:56.csv"),
    ql_pow_err_replan_ramp = pd.read_csv("data/robot/extra/Q_lambda - Power, Error, Rampling Replan - training 2020-08-29 20:44:49.csv"),
    ql_weighted_pow_1_10   = pd.read_csv("data/robot/extra/Q_lambda - Weighted Power_Error (1:10) - training 2020-08-30 00:51:35.csv"),
    ql_moving_avg          = pd.read_csv("data/robot/extra/Q_lambda - Error and 10-Sample Moving Power Average - training 2020-08-29 13:30:58.csv"),
    offset=1
)
f_lr.suptitle(
    "Q Difference\n  Q                                           Q(λ)",
    fontsize=20
)
f_lr.tight_layout()

# Save...
plt.savefig("plots/comparison_q-ql_training_qdiff.eps", format="eps", bbox_inches="tight")
# ...and show
plt.show()

# %%
