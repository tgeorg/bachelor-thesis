#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# %%

# For plotting
import matplotlib.pyplot as plt
from matplotlib import collections  as mc

import numpy as np

# %%

# Initial path
my_path = [(4, 2), (4, 3), (4, 4), (5, 4), (5, 5), (6, 5), (7, 5), (7, 6), (7, 7), (7, 8), (6, 8), (5, 8), (5, 9), (4, 9), (3, 9), (3, 8), (3, 7), (4, 7)]

# Move path one to left, one to the bottom
my_path = [(a-1, b-1) for a, b in my_path]

# Center
my_path = [(a + 0.5, b + 0.5) for a, b in my_path]

# Generate lines
my_lines = list()
for i in range(len(my_path)-1):
    my_lines.append([my_path[i], my_path[i+1]])

# Generate arrows
my_arrows = list()

for i in range(len(my_path)-1):
    my_arrows.append([my_path[i], 
        (my_path[i+1][0] - my_path[i][0], my_path[i+1][1] - my_path[i][1])
    ])
my_arrows

# %%
def add_arrows(ax, amount=len(my_arrows), λ=1):
    og_λ = λ
    λ = 1
    linewidth = 0.001
    headwidth = 0.1
    for i in range(amount):
        ax.arrow(
            my_arrows[-i-1][0][0], my_arrows[-i-1][0][1], λ*my_arrows[-i-1][1][0], λ*my_arrows[-i-1][1][1],
            head_width=λ*headwidth,
            width=λ*linewidth,
            length_includes_head=True,
            color='C1'
        )
        λ = λ*og_λ


# %%
fig, axs = plt.subplots(1, 4) # (rows col)
fig.set_size_inches(20, 5)

# Rows and cols
# 8x10
ncols = 8
nrows = 10

for ax in axs:
    # Set limits
    ax.set_xlim(left=0, right=ncols)
    ax.set_ylim(bottom=0, top=nrows)
    # Display grid
    ax.set_xticks(np.arange(ncols), minor=True)
    ax.set_yticks(np.arange(nrows), minor=True)
    ax.grid(which="major")
    ax.grid(which="minor")
    ax.tick_params(which="minor", size=0)
    # Maintain 1:1 aspect ratio
    ax.set_aspect('equal')
    # Display goal
    ax.text(my_path[-1][0], my_path[-1][1], "G", fontsize=20)#, horizontalalignment='center', verticalalignment='center')
    # Turn off ticks and labels
    ax.tick_params(
        top=False,
        bottom=False,
        left=False,
        right=False,
        labelleft=False,
        labelbottom=False
    )

# Add titles
title_padding = 23
title_font_size = 25
axs[0].set_title("Path taken", wrap=True, fontsize=title_font_size, pad=title_padding)
axs[1].set_title("Action values increased\nby one-step SARSA", wrap=True, fontsize=title_font_size, pad=title_padding)
axs[2].set_title("Action values increased\nby 10-step SARSA", wrap=True, fontsize=title_font_size, pad=title_padding)
axs[3].set_title("Action values increased\nby SARSA(λ) with λ=0.9", wrap=True, fontsize=title_font_size, pad=title_padding)

# Add xlabels
axs[0].set_xlabel("(a)", fontsize=20)
axs[1].set_xlabel("(b)", fontsize=20)
axs[2].set_xlabel("(c)", fontsize=20)
axs[3].set_xlabel("(d)", fontsize=20)

# Add lines
axs[0].add_collection(mc.LineCollection(my_lines))
axs[0].arrow(
    my_arrows[-1][0][0], my_arrows[-1][0][1], my_arrows[-1][1][0], my_arrows[-1][1][1],
    head_width=0.1,
    length_includes_head=True,
    color='C0'
)


# Add arrows
add_arrows(axs[1], amount=1)
add_arrows(axs[2], amount=10)
add_arrows(axs[3], λ=0.9)

# Set to tight layout
fig.tight_layout(pad=-8)

# Save
plt.savefig("plots/saras_n-step_lambda_action_comparison.eps", format="eps", bbox_inches="tight")
#Show
plt.show()
# %%
